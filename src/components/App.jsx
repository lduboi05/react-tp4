import React from 'react'

import Home from '../pages/Home'
import Layout from './Layout'

const App = () => {

    return <Layout>
        <Home />
    </Layout>
}

export default App